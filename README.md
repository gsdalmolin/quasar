## Operacion Fuego de Quasar

## Tecnologias Utilizadas
* PHP
* Composer
* Laravel
* MySQL

## Servidor
https://www.heroku.com/

**Sitio publicado en:**
https://quasar-gd.herokuapp.com

## Preparando ambiente de desarrollo
**Comandos**

* `composer global require laravel/installer`
* `composer create-project --prefer-dist laravel/laravel quasar`
* `php artisan serve`

## Archivos principales del proyecto
**La mayoría de la programacion importante y logica del problema se encuentra en los siguientes archivos:**
- `routes/api.php`
- `app/Http/Controllers/LocationController.php`
- `database/migrations/2021_02_17_162444_create_locations_table.php`
- `database/migrations/2021_02_17_212741_create_satellites_table.php`
- `tests/Features/ExampleTest.php`

## TESTs Automatizados
**Comando:**
* `php artisan test`

**Salida:**
PASS  Tests\Feature\ExampleTest
* ✓ nivel 2  ok 1
* ✓ nivel 2  ok 2
* ✓ nivel 3 kenobi
* ✓ nivel 3 skywalker
* ✓ nivel 3 sato
* ✓ nivel 3 get kenobi
* ✓ nivel 3 get skywalker
* ✓ nivel 3 get sato

Tests:  8 passed
Time:   9.14s

## TESTs Nivel 2: ejecutados en consola linux utilizando comando curl (Client URL)

```
curl --location --request POST 'https://quasar-gd.herokuapp.com/api/topsecret' \
--header 'Content-Type: application/json' \
--data-raw '{
    "satellites": [
	{"name":"kenobi","distance":"100.0","message":"[\"este\", \"\", \"\", \"mensaje\", \"\"]"},
	{"name":"skywalker","distance":"115.5","message":"[\"\", \"es\", \"\", \"\", \"secreto\"]"},
	{"name":"sato","distance":"142.7","message":"[\"este\", \"\", \"un\", \"\", \"\"]"}]
}'

curl --location --request POST 'https://quasar-gd.herokuapp.com/api/topsecret' \
--header 'Content-Type: application/json' \
--data-raw '{
    "satellites": [
	{"name":"kenobi","distance":"806.225","message":"[\"\", \"\", \"\", \"mensaje\", \"\"]"},
	{"name":"skywalker","distance":"860.232","message":"[\"\", \"es\", \"\", \"\", \"secreto\"]"},
	{"name":"sato","distance":"1029.563","message":"[\"\", \"\", \"un\", \"\", \"\"]"}]
}'

curl --location --request POST 'https://quasar-gd.herokuapp.com/api/topsecret' \
--header 'Content-Type: application/json' \
--data-raw '{
    "satellites": [
	{"name":"kenobi","distance":"806.225","message":"[\"este\", \"\", \"\", \"mensaje\", \"\"]"},
	{"name":"skywalker","distance":"860.232","message":"[\"\", \"es\", \"\", \"\", \"secreto\"]"},
	{"name":"sato","distance":"1029.563","message":"[\"este\", \"\", \"un\", \"\", \"\"]"}]
}'

curl --location --request POST 'https://quasar-gd.herokuapp.com/api/topsecret' \
--header 'Content-Type: application/json' \
--data-raw '{
    "satellites": [
	{"name":"kenobi","distance":"538.516","message":"[\"\", \"este\", \"es\", \"un\", \"mensaje\"]"},
	{"name":"skywalker","distance":"565.685","message":"[\"este\", \"\", \"un\", \"mensaje\", \"\"]"},
	{"name":"sato","distance":"824.621","message":"[\"\", \"es\", \"es\", \"\", \"secreto\"]"}]
}'
```


## TESTs Nivel 3: ejecutados en consola linux utilizando comando curl (Client URL)

**Metodos GET sin haber insertado datos con POST**

```
curl --location --request GET 'https://quasar-gd.herokuapp.com/api/topsecret_split/{kenobi}'

curl --location --request GET 'https://quasar-gd.herokuapp.com/api/topsecret_split/{skywalker}'

curl --location --request GET 'https://quasar-gd.herokuapp.com/api/topsecret_split/{sato}'
```


**Metodos  POST para insertar/actualizar datos en la base**

```
curl --location --request POST 'https://quasar-gd.herokuapp.com/api/topsecret_split/kenobi' \
--header 'Content-Type: application/json' \
--data-raw '{
    "distance":"806.225",
    "message":"[\"\", \"este\", \"es\", \"un\", \"mensaje\"]"
}'

curl --location --request POST 'https://quasar-gd.herokuapp.com/api/topsecret_split/skywalker' \
--header 'Content-Type: application/json' \
--data-raw '{
    "distance":"860.232",
    "message":"[\"este\", \"\", \"un\", \"mensaje\", \"\"]"
}'

curl --location --request POST 'https://quasar-gd.herokuapp.com/api/topsecret_split/sato' \
--header 'Content-Type: application/json' \
--data-raw '{
    "distance":"1029.563",
    "message":"[\"\", \"es\", \"es\", \"\", \"secreto\"]"
}'
```


**Metodos GET luego de haber insertado datos con POST**

```
curl --location --request GET 'https://quasar-gd.herokuapp.com/api/topsecret_split/{kenobi}'

curl --location --request GET 'https://quasar-gd.herokuapp.com/api/topsecret_split/{skywalker}'

curl --location --request GET 'https://quasar-gd.herokuapp.com/api/topsecret_split/{sato}'
```


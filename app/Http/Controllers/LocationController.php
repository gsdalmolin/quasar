<?php

namespace App\Http\Controllers;

use App\Location;
use App\Satellite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Integer;

class LocationController extends Controller
{
    /********************************************
     **************** NIVEL 2 *******************
     ********************************************
     */

    /**
     * FUNCION PRINCIPAL
     * input: json request
     * output: json response
     */
    public function topSecret(Request $request)
    {
        $data = $request->all();

        //Distancias de la nave origen a cada satelite (radios)
        $dist_kenobi = $data['satellites'][0]['distance'];
        $dist_skywalker = $data['satellites'][1]['distance'];
        $dist_sato = $data['satellites'][2]['distance'];

        //Funcion para obtener la posicion de la nave
        $position = $this->GetLocation($dist_kenobi, $dist_skywalker, $dist_sato);

        //Mensajes de la nave origen a cada satelite
        $msg_kenobi = json_decode($data['satellites'][0]['message']);
        $msg_skywalker = json_decode($data['satellites'][1]['message']);
        $msg_sato = json_decode($data['satellites'][2]['message']);

        //Funcion para obtener el mensaje completo de la nave
        $message = $this->GetMessage($msg_kenobi, $msg_skywalker,$msg_sato);

        // Guardo los datos de la llamada y los resultados
        Location::insert([
            'satellites_data' => json_encode($data['satellites']),
            'position' => json_encode($position),
            'message' => $message,
            'created_at' => now(),
            'updated_at' => now()]);

        // Si los datos contienen informacion fidedigna retorno posicion y mensaje con codigo 200
        // Sino retorno un string indicando que no se pudo resolver y codigo 404
        if ($position['x'] != -1 && $position['y'] != -1 && $message != "-1"){
            return response(json_encode(array('position' => $position,'message' => $message)))
                ->setStatusCode(200);
        } else {
            return response(json_encode(array("ERROR: No se pudo determinar la posicion o el mensaje.")))
                ->setStatusCode(404);
        }
    }

    /**
     * FUNCION PARA OBTENER LA POSICION DEL EMISOR
     * input: distancia al emisor tal cual se recibe en cada satelite
     * output: las coordenadas ‘x’ e ‘y’ del emisor del mensaje
     */
    protected function GetLocation(float $dist_kenoby, float $dist_skywalker, float $dist_sato){

        // Satelite Kenobi: posicion en el sistema original
        $x_kenobi = -500;
        $y_kenobi = -200;
        // Satelite Skywalker: posicion en el sistema original
        $x_skywalker = 100;
        $y_skywalker = -100;
        // Satelite Sato: posicion en el sistema original
        $x_sato = 500;
        $y_sato = 100;

        // la estrategia para obtener la posicion de la nave consiste en 5 pasos:
        // 1) Posicion relativa de la nave en un sistema de coordenadas determinado por 2 satelites.
        $pos_relative = $this->RelativePosition($dist_kenoby,$dist_skywalker,$x_kenobi,$y_kenobi,$x_skywalker,$y_skywalker);

        // 2) Angulo entre 2 satelites en el sistema de coordenadas original.
        $angle = $this->Angle($x_kenobi,$y_kenobi,$x_skywalker,$y_skywalker);

        // 3) Rotar el sistema de coordenadas determinado por los 2 satelites.
        $pos_rotate = $this->RotatePosition($pos_relative['x'],$pos_relative['y'],$angle);

        // 4) Trasladar el sistema de coordenadas con respecto al satelite del origen del sistema anterior.
        $position = $this->TranslatePosition($pos_rotate['x'],$pos_rotate['y'], $x_kenobi, $y_kenobi);

        // 5) Verificar las coordenadas con el 3er satelite.
        // Llamo a las mismas funciones anteriores, pero considerando otros 2 satelites para ver si coinciden
        $verif_relative = $this->RelativePosition($dist_skywalker,$dist_sato,$x_skywalker,$y_skywalker,$x_sato,$y_sato);
        $verif_angle = $this->Angle($x_skywalker,$y_skywalker,$x_sato,$y_sato);
        $verif_rotate = $this->RotatePosition($verif_relative['x'],$verif_relative['y'],$verif_angle);
        $verif_position = $this->TranslatePosition($verif_rotate['x'],$verif_rotate['y'], $x_skywalker, $y_skywalker);

        // Compruebo si la triangulacion coincide
        if ($position['x'] == $verif_position['x'] && $position['y'] == $verif_position['y']){
            $output= $position;
        } else{
            $output= array('x' => -1, 'y' => -1);
        }

        return $output;
    }

    // 1) Posicion relativa
    protected function RelativePosition(float $r1, float $r2, float $x1, float $y1, float $x2, float $y2){
        // Distancia entre satelites
        $dis = sqrt(pow(($x2-$x1),2)+pow(($y2-$y1),2));

        // Coordenadas x e y
        $xre = (pow($r1, 2) - pow($r2, 2) + pow($dis,2) )/(2*$dis);
        $yre = sqrt(pow($r1,2) - pow((pow($r1,2) - pow($r2,2) + pow($dis,2)),2)/(4*pow($dis,2)));

        return array('x' => $xre, 'y' => $yre);
    }

    // 2) Angulo entre satelites
    protected function Angle(float $x1, float $y1, float $x2, float $y2){
        // Pendiente
        $pe = ($y2-$y1)/($x2-$x1);

        // Angulo en radianes
        return atan($pe);
    }

    // 3) Rotar sistema de coordenadas
    protected function RotatePosition(float $xre, float $yre, float $angle){
        $xro = $xre*cos($angle) - $yre*sin($angle);
        $yro = $yre*cos($angle) + $xre*sin($angle);

        //Los datos de entrada tienen 3 decimales, por eso redondeo a 2
        return array('x' => round($xro, 2), 'y' => round($yro,2));
    }

    // 4) Trasladar el sistema de coordenadas
    protected function TranslatePosition(float $xro, float $yro, float $x1, float $y1){
        $x_final = $xro + $x1;
        $y_final = $yro + $y1;

        return array('x' => $x_final, 'y' => $y_final);
    }

    /**
     * FUNCION PARA OBTENER EL MENSAJE DEL EMISOR
     * input: el mensaje tal cual es recibido en cada satelite
     * output: el mensaje tal cual lo genera el emisor del mensaje
     */
    protected function GetMessage(array $msg_kenoby, array $msg_skywalker, array $msg_sato){
        $o_message = array();
        $output = "";

        // Tamaño maximo de los array para iterar
        $max_size_array = max(count($msg_kenoby),count($msg_skywalker),count($msg_sato));

        // Comparo cada palabra en la misma posicion.
        for ($i=0; $i < $max_size_array; $i++){
            $word = "";
            if(strlen($msg_kenoby[$i]) != 0 && $msg_kenoby[$i] != $word && !in_array($msg_kenoby[$i], $o_message)){
                $word = $msg_kenoby[$i];
            } elseif(strlen($msg_skywalker[$i]) != 0 && $msg_skywalker[$i] != $word && !in_array($msg_skywalker[$i], $o_message)){
                $word = $msg_skywalker[$i];
            } elseif (strlen($msg_sato[$i]) != 0 && $msg_sato[$i] != $word && !in_array($msg_sato[$i], $o_message)){
                $word = $msg_sato[$i];
            }

            // Concateno cada palabra para determinar el mensaje.
            array_push($o_message,$word);
        }

        // Validacion del mensaje
        for ($i=0; $i < $max_size_array; $i++){
            if ($o_message[$i] == ""){
                $output = "-1";
            }
        }

        // Convierto el array en string
        if ($output != "-1"){
            for ($i=0; $i < $max_size_array; $i++){
                if ($i == 0){
                    $output = $output . $o_message[$i];
                } else {
                    $output = $output . " " . $o_message[$i];
                }
            }
        }

        return $output;
    }

    /********************************************
     **************** NIVEL 3 *******************
     ********************************************
     */

    /**
     * CADA SATELITE LLAMA A LA MISMA FUNCION PARA ALMACENAR LOS DATOS
     * input: json request
     * output: resultado y codigo de respuesta
     */
    public function store_kenobi(Request $request){

        return $this->store_satellite("kenobi",$request['distance'],$request['message']);;
    }

    public function store_skywalker(Request $request){

        return $this->store_satellite("skywalker",$request['distance'],$request['message']);;
    }

    public function store_sato(Request $request){

        return $this->store_satellite("sato",$request['distance'],$request['message']);;
    }

    /**
     * FUNCION PARA ALMACENAR LOS DATOS
     * input: nombre del satelite, distancia y mensaje
     * output: resultado y codigo de respuesta
     */
    private function store_satellite(string $name, float $distance, string $message)
    {
        $satellite = DB::table('satellites')->where('name',$name)->first();

        // Si no existe el satellite lo inserto, sino, lo actualizo, en caso contrario devuelvo ERROR.
        if($satellite == null){
            DB::table('satellites')->insert([
                'name' => $name,
                'distance' => $distance,
                'message' => $message,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')]);

            return response(json_encode(array('Datos insertados.')))
                ->setStatusCode(200);

        } elseif ($satellite != null) {
            DB::table('satellites')
                ->where('name',$name)
                ->update([
                    'distance' => $distance,
                    'message' => $message,
                    'updated_at' => date('Y-m-d H:i:s')]);

            return response(json_encode(array('Datos actualizados.')))
                ->setStatusCode(200);

        } else {
            return response(json_encode(array('ERROR DE DATOS.')))
                ->setStatusCode(404);

        }
    }

    /**
     * FUNCION PARA CALCULAR LA POSICION Y EL MENSAJE
     * input: nombre del satelite
     * output: resultado y codigo de respuesta
     */
    public function calculate_data(string $satellite_name)
    {
        $satellite = DB::table('satellites')->where('name',$satellite_name)->get();
        $satellites = Satellite::select('name','distance','message')->get();

        // Si no existen datos del satellite o no tengo suficiente informacion para el calculo retorno ERROR
        if($satellite == null || count($satellites) != 3){
            return response(json_encode(array("ERROR: No se puede determinar la posicion o el mensaje.")))
                ->setStatusCode(404);
        }

        // Identifico las variables para pasarlos como argumentos de las llamadas de funciones
        for ($i=0; $i < count($satellites); $i++){
            // Datos de Kenobi
            if($satellites[$i]['name'] == "kenobi"){
                $dist_kenobi = $satellites[$i]['distance'];
                $msg_kenobi = json_decode($satellites[$i]['message']);
            }
            // Datos de Skywalker
            if($satellites[$i]['name'] == "skywalker"){
                $dist_skywalker = $satellites[$i]['distance'];
                $msg_skywalker = json_decode($satellites[$i]['message']);
            }
            // Datos de Sato
            if($satellites[$i]['name'] == "sato"){
                $dist_sato = $satellites[$i]['distance'];
                $msg_sato = json_decode($satellites[$i]['message']);
            }
        }

        //Funcion para obtener la posicion de la nave
        $position = $this->GetLocation($dist_kenobi, $dist_skywalker, $dist_sato);

        //Funcion para obtener el mensaje completo de la nave
        $message = $this->GetMessage($msg_kenobi,$msg_skywalker,$msg_sato);

        // Guardo los datos de la llamada y los resultados
        Location::insert([
            'satellites_data' => json_encode($satellites),
            'position' => json_encode($position),
            'message' => $message,
            'created_at' => now(),
            'updated_at' => now()]);

        // Si los datos contienen informacion fidedigna retorno posicion y mensaje con codigo 200
        // Sino retorno un string indicando que no se pudo resolver y codigo 404
        if ($position['x'] != -1 && $position['y'] != -1 && $message != "-1"){
            return response(json_encode(array('position' => $position,'message' => $message)))
                ->setStatusCode(200);
        } else {
            return response(json_encode(array("ERROR: No se pudo determinar la posicion o el mensaje.")))
                ->setStatusCode(404);
        }
    }
}

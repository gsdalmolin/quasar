<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// NIVEL 2
Route::post('/topsecret', 'LocationController@topSecret');

// NIVEL 3
// En los metodos POST almaceno la informacion en la base de datos
Route::post('/topsecret_split/kenobi','LocationController@store_kenobi');
Route::post('/topsecret_split/skywalker','LocationController@store_skywalker');
Route::post('/topsecret_split/sato','LocationController@store_sato');

// En el metodo GET calculo la distancia y el mensaje si estan almacenados en la base de datos
Route::get('/topsecret_split/{satellite_name}', 'LocationController@calculate_data');

<?php

namespace Tests\Feature;

use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * Test Nivel 2
     * Datos correctos, posicion y mensajes calculables
     * Codigo de estado: 200
     */
    public function test_Nivel_2_Ok_1()
    {
        $data = array("satellites" => array(
            array("name" => "kenobi", "distance" => 806.225, "message" => json_encode(array("", "este", "es", "un", "mensaje"))),
            array("name" => "skywalker", "distance" => 860.232, "message" => json_encode(array("este", "", "un", "mensaje", ""))),
            array("name" => "sato", "distance" => 1029.563, "message" => json_encode(array("", "es", "es", "", "secreto")))));

        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->post('https://quasar-gd.herokuapp.com/api/topsecret',
            ['form_params' => $data]
        );

        if ($response->getStatusCode() == 200){
            $this->expectNotToPerformAssertions();
            echo $response->getBody()->getContents();
        }

    }

    public function test_Nivel_2_Ok_2()
    {
        $data = array("satellites" => array(
            array("name" => "kenobi", "distance" => 538.516, "message" => json_encode(array("este","","","mensaje",""))),
            array("name" => "skywalker", "distance" => 565.685, "message" => json_encode(array("","es","","","secreto"))),
            array("name" => "sato", "distance" => 824.621, "message" => json_encode(array("este","","un","","")))));

        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        //$response = $client->post(env('APP_URL').':8000/api/topsecret',
        $response = $client->post('https://quasar-gd.herokuapp.com/api/topsecret',
            ['form_params' => $data]
        );

        if ($response->getStatusCode() == 200){
            $this->expectNotToPerformAssertions();
            echo $response->getBody()->getContents();
        }

    }

    /**
     * Test Nivel 3 - Satelite Kenobi
     * Datos  insertados o actualizados
     * Codigo de estado: 200
     */
    public function test_Nivel_3_kenobi()
    {
        $data = array(
            "distance" => 806.225,
            "message" => json_encode(array("", "este", "es", "un", "mensaje")));

        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->post('https://quasar-gd.herokuapp.com/api/topsecret_split/kenobi',
            ['form_params' => $data]
        );

        if ($response->getStatusCode() == 200){
            $this->expectNotToPerformAssertions();
            echo $response->getBody()->getContents();
        }

    }

    /**
     * Test Nivel 3 - Satelite Skywalker
     * Datos  insertados o actualizados
     * Codigo de estado: 200
     */
    public function test_Nivel_3_skywalker()
    {
        $data = array(
            "distance" => 860.232,
            "message" => json_encode(array("este", "", "un", "mensaje", "")));

        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->post('https://quasar-gd.herokuapp.com/api/topsecret_split/skywalker',
            ['form_params' => $data]
        );

        if ($response->getStatusCode() == 200){
            $this->expectNotToPerformAssertions();
            echo $response->getBody()->getContents();
        }

    }

    /**
     * Test Nivel 3 - Satelite Sato
     * Datos  insertados o actualizados
     * Codigo de estado: 200
     */
    public function test_Nivel_3_sato()
    {
        $data = array(
            "distance" => 1029.563,
            "message" => json_encode(array("", "es", "es", "", "secreto")));

        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->post('https://quasar-gd.herokuapp.com/api/topsecret_split/sato',
            ['form_params' => $data]
        );

        if ($response->getStatusCode() == 200){
            $this->expectNotToPerformAssertions();
            echo  $response->getBody()->getContents();
        }
    }

    /**
     * Test Nivel 3 - GET Kenobi
     * Codigo de estado: 200
     */
    public function test_Nivel_3_get_kenobi()
    {
        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->get('https://quasar-gd.herokuapp.com/api/topsecret_split/kenobi');

        if ($response->getStatusCode() == 200){
            $this->expectNotToPerformAssertions();
            echo  $response->getBody()->getContents();
        }
    }

    /**
     * Test Nivel 3 - GET Skywalker
     * Codigo de estado: 200
     */
    public function test_Nivel_3_get_skywalker()
    {
        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->get('https://quasar-gd.herokuapp.com/api/topsecret_split/skywalker');

        if ($response->getStatusCode() == 200){
            $this->expectNotToPerformAssertions();
            echo  $response->getBody()->getContents();
        }
    }

    /**
     * Test Nivel 3 - GET Sato
     * Codigo de estado: 200
     */
    public function test_Nivel_3_get_sato()
    {
        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

        $response = $client->get('https://quasar-gd.herokuapp.com/api/topsecret_split/sato');

        if ($response->getStatusCode() == 200){
            $this->expectNotToPerformAssertions();
            echo  $response->getBody()->getContents();
        }
    }
}
